const socket = io()

const seconds = document.querySelector('#seconds')

const timerBtn = document.querySelector('#timerBtn')
// let flag

let time = 5
let interval
let interval2

// socket.emit('start')
// const { userName, room } = Qs.parse(location.search, { ignoreQueryPrefix: true })
// socket.emit('join', { userName, room }, (error) => {
//   if (error) {
//     alert(error)
//     location.href = '/'
//   }
// })

// socket.on('time', (data) => {
//   console.log('time event', data)
//   seconds.innerHTML = data
// })
function startInterval() {
  interval = setInterval(() => {
    if (time <= 0) {
      seconds.innerHTML = ''
      time = 10
      clearTimer(interval)
      return socket.emit('stop', interval)
    }
    socket.emit('timer', time, interval)
    console.log('after timer event', interval)
    time--
  }, 1000)
}

function clearTimer(intervalId) {
  console.log(intervalId)
  clearInterval(intervalId)
  // console.log(intervals + 'before ')
  // intervals.splice(0, intervals.length)
  // console.log(intervals + 'after ')
  time = 5
  seconds.innerHTML = ''
}

socket.on('start', () => {
  // clearTimer(interval);
  // intervals.push(interval)
  interval2 = interval
  startInterval()
})

socket.on('timer', (time, intervalId) => {
  // intervals.push(intervalId)
  interval2 = intervalId
  seconds.innerHTML = time
})

socket.on('stop', (interval) => {
  clearTimer(interval)
})

// socket.on('roomTimer', (data) => {
//   console.log(data)
// })

timerBtn.addEventListener('click', function () {
  // socket.emit('startTimer')
  // console.log(interval)
  if (interval) {
    clearTimer(interval)
  }
  socket.emit('click', interval2)

  // flag = false
  // timerBtn.disabled = false
  // flag = true
  // if (flag === true) {
  //   timerBtn.disabled = true
  //   flag = false
  //   // socket.emit('stop')
  // }
})
