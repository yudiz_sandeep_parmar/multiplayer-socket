require('../env')
const path = require('path')
const express = require('express')
const app = express()
const http = require('http').createServer(app)
// const { addUser, removeUser, getUser, getUsersInRoom, users } = require('./utils/users')
const io = require('socket.io')(http)
const client = []
let intervalId

const publicDirectoryPath = path.join(__dirname, '../public')
app.use(express.static(publicDirectoryPath))

io.on('connection', socket => {
  client.push(socket.id)
  console.log(`User ${client} connected`)
  // socket.on('join', (options, callback) => {
  //   const { error, user } = addUser({ id: socket.id, ...options })
  //   // client[socket.id] = { id: socket.id, turn: false }
  //   if (error) {
  //     return callback(error)
  //   }
  //   // if (users.length < 1) {
  //   //   throw new Error('room is full')
  //   // }
  //   console.log(users)
  //   // socket.join(user.room)
  //   // io.to(users[0].room).emit('roomTimer', users[0].room)
  //   // io.to(user.room).emit('roomData', {
  //   //   room: user.room,
  //   //   users: getUsersInRoom(user.room)
  //   // })
  // })
  if (!intervalId && client.length > 1) {
    socket.broadcast.emit('start')
  }

  socket.on('timer', (time, interval) => {
    intervalId = interval
    socket.broadcast.emit('timer', time, interval)
  })

  socket.on('stop', (interval) => {
    socket.broadcast.emit('stop', interval)
    socket.broadcast.emit('start', interval)
  })

  socket.on('click', (interval) => {
    socket.broadcast.emit('stop', interval)
    console.log('yo!')
    socket.emit('start', interval)
  })
  socket.on('disconnect', () => {
    // const user = removeUser(socket.id)
    console.log(`User ${socket.id} left`)
    client.splice(client.indexOf(socket.id), 1)
    // if (user) {
    //   io.to(user.room).emit('roomData', {
    //     room: user.room,
    //     users: getUsersInRoom(user.room)
    //   })
    // }
  })
})

http.listen(process.env.PORT, () => {
  console.log(`Magic happens on ${process.env.PORT}`)
})
